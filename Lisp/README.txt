
README.txt

mst.lisp

Autore: Marco Magni (matricola 851810)


"Graph algorithms: minimum spanning trees"

Scopo del progetto è l'implementazione dell'algoritmo di Prim per la soluzione del problema MST per grafi non-diretti e connessi con pesi non negativi.
Di seguito le indicazioni per poter utilizzare l'API risolutiva in Common Lisp.


Interfaccia Common Lisp per la manipolazione di grafi.

	is-graph
		parametri: graph-id
		ritorna: graph-id o NIL
		comportamento: se il grafo esiste già ritorna graph-id, altrimenti ritorna NIL

	new-graph
		parametri: graph-id
		ritorna: graph-id
		comportamento: crea un grafo e lo inserisce nel sistema; se il grafo esiste già non fa nulla

	delete-graph
		parametri: graph-id
		ritorna: NIL
		comportamento: rimuove l'intero grafo (vertici e archi compresi) dal sistema; se il grafo non esiste non fa nulla

	new-vertex
		parametri: graph-id, vertex-id
		ritorna: vertex-rep
		comportamento: aggiunge un nuovo vertice vertex-id del grafo graph-id nel sistema; se il grafo non esiste lo crea

	graph-vertices
		parametri: graph-id
		ritorna: vertex-rep-list
		comportamento: ritorna una lista dei vertici del grafo

	new-arc
		parametri: graph-id, vertex-id, vertex-id, [weight = 1]
		ritorna: arc-rep
		comportamento: aggiunge un nuovo arco vertex-id del grafo graph-id nel sistema; se il grafo o i vertici non esistono vengono creati

	graph-arcs
		parametri: graph-id
		ritorna:	arc-rep-list
		comportamento: ritorna una lista degli archi del grafo

	graph-vertex-neighbors
		parametri: graph-id, vertex-id
		ritorna: arc-rep-list
		comportamento: ritorna una lista degli archi che portano ai vertici immediatamente raggiungibili dal vertice vertex-id

	graph-vertex-adjacent
		parametri: graph-id, vertex-id
		ritorna: vertex-rep-list
		comportamento: ritorna una lista dei vertici adiacenti al vertice vertex-id

	graph-print
		parametri: graph-id
		ritorna: T
		comportamento: stampa alla console dell'interprete una lista dei vertici e degli archi del grafo graph-id


MST in Common Lisp.
	
	mst-vertex-key
		parametri: graph-id, vertex-id
		ritorna: key
		comportamento: dato un vertex-id di un grafo graph-id ritorna, durante e dopo l'esecuzione dell'algoritmo di Prim, il peso minimo di un arco che connette vertex-id nell'albero minimo; se questo arco non esiste (ed all'inizio dell'esecuzione) allora key è MOST-POSITIVE-DOUBLE-FLOAT

	mst-previous
		parametri: graph-id, vertex-id-v
		ritorna: vertex-id-u
		comportamento: ritorna, durante e dopo l'esecuzione dell'algoritmo di Prim, il vertice vertex-id-u genitore di vertex-id-v nell'albero minimo; se vertex-id-v è la radice dell'albero allora vertex-id-u è NIL

	mst-prim
		parametri: graph-id, source
		ritorna: NIL
		comportamento: dopo la sua esecuzione, le hash-table *vertex-key*  e *previous* contengono le associazioni di distanza e vertice genitore per ogni vertice appartenente al grafo graph-id calcolate tramite l'algoritmo di Prim partendo dal vertice source

	mst-get
		parametri: graph-id, source
		ritorna: preorder-mst
		comportamento: ritorna una lista degli archi del MST ordinata secondo un attraversamento preorder dello stesso, fatta rispetto al peso dell'arco; archi con pari peso vengono ordinati lessicograficamente rispetto al nome del vertice target


MinHeap in Common Lisp.

	new-heap
		parametri: heap-id, [capacity = 42]
		ritorna: heap-rep
		comportamento: inserisce nel sistema un nuovo heap heap-id con capacità massima capacity

	heap-id
		parametri: heap-rep
		ritorna: heap-id
		comportamento: data una rappresentazione heap-rep di uno heap, ritorna l'heap-id dello heap

	heap-size
		parametri: heap-rep
		ritorna: heap-size
		comportamento: data una rappresentazione heap-rep di uno heap, ritorna la dimensione attuale dello heap

	heap-actual-heap
		parametri: heap-rep
		ritorna: actual-heap
		comportamento: data una rappresentazione heap-rep di uno heap, ritorna i valori attualmente contenuti nello heap

	heap-delete
		parametri: heap-id
		ritorna: T
		comportamento: rimuove tutto lo heap heap-id dal sistema; se heap-id non esiste non fa nulla

	heap-empty
		parametri: heap-id
		ritorna: boolean
		comportamento: ritorna T se lo heap heap-id esiste ed è vuoto; altrimenti NIL

	heap-not-empty
		parametri: heap-id
		ritorna: boolean
		comportamento: ritorna T se lo heap heap-id esiste e non è vuoto; altrimenti NIL

	heap-head
		parametri: heap-id
		ritorna: (key, value)
		comportamento: ritorna la coppia (key, value) con chiave minima nello heap heap-id

	heap-insert
		parametri: heap-id, key, value
		ritorna: boolean
		comportamento: inserisce nello heap heap-id l'elemento value con chiave key; ritorna T se l'inserimento ha avuto successo, altrimenti NIL

	heap-extract
		parametri: heap-id
		ritorna: (key, value)
		comportamento: estrae dallo heap e ritorna la coppia (key, value) con chiave minima 

	heap-modify-key
		parametri: heap-id, new-key, old-key, value
		ritorna: boolean
		comportamento: sostituisce la chiave old-key associata a value con new-key; ritorna T se la modifica ha avuto successo, altrimenti NIL

	heap-print
		parametri: heap-id
		ritorna:	boolean
		comportamento: stampa sulla console lo stato interno dello heap heap-id; ritorna true se heap-id esiste, altrimenti NIL


