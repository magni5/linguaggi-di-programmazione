%%%% -*- Mode: Prolog -*-

%%%% mst.pl
%%%% Magni Marco 851810


%%% Librerie

:- use_module(library(listing)).
:- use_module(library(csv)).


%%% Predicati dinamici

:- dynamic graph/1.
:- dynamic vertex/2.
:- dynamic arc/4.
:- dynamic arc/3.
:- dynamic vertex_key/3.
:- dynamic vertex_previous/3.
:- dynamic heap/2.
:- dynamic heap_entry/4.


%%% Manipolazione di grafi

new_graph(G) :-
    graph(G),
    !.
new_graph(G) :-
    assert(graph(G)),
    !.

delete_graph(G) :-
    retractall(arc(G, _, _, _)),
    retractall(vertex(G, _)),
    retractall(graph(G)).

new_vertex(G, V) :-
    vertex(G, V),
    !.
new_vertex(G, V) :-
    new_graph(G),
    assert(vertex(G, V)),
    !.

graph_vertices(G, Vs) :-
    graph(G),
    findall(vertex(G, X), vertex(G, X), Vs).

list_vertices(G) :-
    graph(G),
    listing(vertex(G, _)).

new_arc(G, U, V, W) :-
    arc(G, U, V, W),
    !.
new_arc(G, U, V, W) :-
    new_vertex(G, U),
    new_vertex(G, V),
    W >= 0,
    U \= V,
    retractall(arc(G, U, V, _)),
    retractall(arc(G, V, U, _)),
    assert(arc(G, U, V, W)),
    !.
new_arc(G, U, V) :-
    new_arc(G, U, V, 1).

graph_arcs(G, Es) :-
    graph(G),
    findall(arc(G, U, V, W), arc(G, U, V, W), Es).

vertex_neighbors(G, V, Ns) :-
    vertex(G, V),
    findall(arc(G, V, U, W), arc(G, V, U, W), Ns1),
    findall(arc(G, V, U, W), arc(G, U, V, W), Ns2),
    append(Ns1, Ns2, Ns).

adjs(G, V, Vs) :-
    vertex(G, V),
    findall(vertex(G, U), arc(G, U, V, _), Vs1),
    findall(vertex(G, U), arc(G, V, U, _), Vs2),
    append(Vs1, Vs2, Vs).

list_arcs(G) :-
    graph(G),
    listing(arc(G, _, _, _)).

list_graph(G) :-
    graph(G),
    list_vertices(G),
    list_arcs(G).

read_graph(G, FileName) :-
    delete_graph(G),
    csv_read_file(FileName, Rows, [separator(0'\t)]),
    read_graph_rows(G, Rows),
    !.

read_graph_rows(_G, []) :-
    !.
read_graph_rows(G, [H | T]) :-
    H = row(U, V, W),
    new_arc(G, U, V, W),
    read_graph_rows(G, T),
    !.

write_graph(G, FileName, Type) :-
    Type = graph,
    findall(row(U, V, W), arc(G, U, V, W), Rs),
    csv_write_file(FileName, Rs, [separator(0'\t)]),
    !.
write_graph(G, FileName, Type) :-
    Type = edges,
    retractall(temp_row(_, _, _)),
    edges_to_rows(G),
    findall(row(U, V, W), temp_row(U, V, W), Rs),
    retractall(temp_row(_, _, _)),
    csv_write_file(FileName, Rs, [separator(0'\t)]),
    !.
write_graph(G, FileName) :-
    write_graph(G, FileName, graph),
    !.

edges_to_rows([]) :-
    !.
edges_to_rows([E | Es]) :-
    E = arc(_, U, V, W),
    assert(temp_row(U, V, W)),
    edges_to_rows(Es).


%%% MST

mst_prim(G, Source) :-
    graph(G),
    findall(X, vertex(G, X), Vs),
    retractall(vertex_key(G, _, _)),
    retractall(vertex_previous(G, _, _)),
    delete_heap(G),
    init_vertices(G, Vs),
    retract(vertex_key(G, Source, inf)),
    assert(vertex_key(G, Source, 0)),
    new_heap(G),
    init_heap(G, Vs),
    modify_key(G, 0, inf, Source),
    mst_prim_execute(G).

mst_get(G, Source, PreorderTree) :-
    vertex_previous(G, Source, nil),
    retractall(temp_get(_)),
    assert(temp_get([])),
    mst_get_execute(G, Source),
    temp_get(PreorderTree),
    retractall(temp_get(_)).
    
init_vertices(_G, []) :-
    !.
init_vertices(G, [V | Vs]) :-
    assert(vertex_key(G, V, inf)),
    assert(vertex_previous(G, V, nil)),
    init_vertices(G, Vs),
    !.

init_heap(_H, []) :-
    !.
init_heap(H, [V | Vs]) :-
    heap_insert(H, inf, V),
    init_heap(H, Vs),
    !.

mst_prim_execute(H) :-
    heap_empty(H),
    !.
mst_prim_execute(H) :-
    heap_extract(H, EK, EV),
    retractall(vertex_key(H, EV, _)),
    assert(vertex_key(H, EV, EK)),
    vertex_previous(H, EV, _),
    vertex_neighbors(H, EV, Ns),
    mst_prim_execute_2(Ns),
    mst_prim_execute(H),
    !.

mst_prim_execute_2([]):-
    !.
mst_prim_execute_2([N | Ns]) :-
    N = arc(H, U, V, W),
    heap_entry(H, _, K, V),
    vertex_previous(H, V, _),
    W < K,
    modify_key(H, W, K, V),
    retractall(vertex_previous(H, V, _)),
    assert(vertex_previous(H, V, U)),
    mst_prim_execute_2(Ns),
    !.
mst_prim_execute_2([_ | Ns]) :-
    mst_prim_execute_2(Ns),
    !.

mst_get_execute(_, []) :-
    !.
mst_get_execute(G, [V | Vs]) :-
    V = sorted_arc(G, W, U, P),
    Vv = arc(G, P, U, W),
    temp_get(PreorderTree),
    append(PreorderTree, [Vv], NewPreorderTree),
    retractall(temp_get(_)),
    assert(temp_get(NewPreorderTree)),
    mst_get_execute(G, U),
    mst_get_execute(G, Vs),
    !.

mst_get_execute(G, V) :-
    findall(sorted_arc(G, W, U, V),
	    (vertex_key(G, U, W), vertex_previous(G, U, V)),
	    SortedArcs),
    sort(SortedArcs, [B | As]),
    B = sorted_arc(G, W, L, V),
    A = arc(G, V, L, W),
    temp_get(PreorderTree),
    append(PreorderTree, [A], NewPreorderTree),
    retractall(temp_get(_)),
    assert(temp_get(NewPreorderTree)),
    mst_get_execute(G, L),
    mst_get_execute(G, As),
    !.
mst_get_execute(_, _) :-
    !.


%%% MinHeap

new_heap(H) :-
    heap(H, _),
    !.
new_heap(H) :-
    assert(heap(H, 0)),
    !.

delete_heap(H) :-
    retractall(heap_entry(H, _, _, _)),
    retractall(heap(H, _)).

heap_has_size(H, S) :-
    heap(H, S).

heap_empty(H) :-
    heap(H, 0).

heap_not_empty(H) :-
    heap(H, S),
    S > 0.

heap_head(H, K, V) :-
    heap_entry(H, 0, K, V).

heap_insert(H, K, V) :-
    heap(H, S),
    assert(heap_entry(H, S, K, V)),
    retract(heap(H, S)),
    SN is (S + 1),
    assert(heap(H, SN)),
    heap_sift(H, S).

heap_extract(H, K, V) :-
    heap(H, S),
    S > 1,
    heap_head(H, K, V),
    retract(heap_entry(H, 0, K, V)),
    SN is (S - 1),
    heap_entry(H, SN, KT, VT),
    assert(heap_entry(H, 0, KT, VT)),
    retract(heap_entry(H, SN, KT, VT)),
    retract(heap(H, S)),
    assert(heap(H, SN)),
    heapify(H, 0),
    !.
heap_extract(H, K, V) :-
    heap(H, S),
    S = 1,
    heap_head(H, K, V),
    retract(heap_entry(H, 0, K, V)),
    retract(heap(H, S)),
    assert(heap(H, 0)),
    !.

modify_key(H, NewKey, OldKey, V) :-
    heap_entry(H, I, OldKey, V),
    retract(heap_entry(H, I, OldKey, V)),
    assert(heap_entry(H, I, NewKey, V)),
    NewKey < OldKey,
    heap_sift(H, I),
    !.
modify_key(H, NewKey, OldKey, V) :-
    heap_entry(H, I, OldKey, V),
    retract(heap_entry(H, I, OldKey, V)),
    assert(heap_entry(H, I, NewKey, V)),
    heapify(H, I),
    !.

list_heap(H) :-
    heap(H, _),
    listing(heap_entry(H, _, _, _)).

build_heap(H) :-
    heap(H, S),
    ST is (floor(S / 2) - 1),
    build_heap(H, ST).

build_heap(_H, I) :-
    I = -1,
    !.
build_heap(H, I) :-
    heapify(H, I),
    IN is (I - 1),
    build_heap(H, IN),
    !.

heapify(_H, I) :-
    I = -1,
    !.
heapify(H, I) :-
    heap(H, S),
    heap_entry(H, I, KP, VP),
    IR is ((2 * I) + 2),
    IR < S,
    IL is ((2 * I) + 1),
    heap_entry(H, IR, KR, VR),
    heap_entry(H, IL, KL, _),
    KR < KP,
    KR < KL,
    retract(heap_entry(H, I, KP, VP)),
    retract(heap_entry(H, IR, KR, VR)),
    assert(heap_entry(H, I, KR, VR)),
    assert(heap_entry(H, IR, KP, VP)),
    heapify(H, IR),
    !.
heapify(H, I) :-
    heap(H, S),
    heap_entry(H, I, KP, VP),
    IL is ((2 * I) + 1),
    IL < S,
    heap_entry(H, IL, KL, VL),
    KL < KP,
    retract(heap_entry(H, I, KP, VP)),
    retract(heap_entry(H, IL, KL, VL)),
    assert(heap_entry(H, I, KL, VL)),
    assert(heap_entry(H, IL, KP, VP)),
    heapify(H, IL),
    !.
heapify(H, I) :-
    IT is (I - 1),
    heapify(H, IT),
    !.

heap_sift(_H, I) :-
    I = 0,
    !.
heap_sift(H, I) :-
    P is (floor((I - 1) / 2)),
    heap_entry(H, I, KI, _),
    heap_entry(H, P, KP, _),
    KP =< KI,
    !.
heap_sift(H, I) :-
    P is (floor((I - 1) / 2)),
    heap_entry(H, I, KI, VI),
    heap_entry(H, P, KP, VP),
    KI < KP,
    retract(heap_entry(H, I, KI, VI)),
    retract(heap_entry(H, P, KP, VP)),
    assert(heap_entry(H, I, KP, VP)),
    assert(heap_entry(H, P, KI, VI)),
    heap_sift(H, P),
    !.


%%%% end of file -- mst.pl --
