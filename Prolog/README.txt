
README.txt

mst.pl

Autore: Marco Magni (matricola 851810)


"Graph algorithms: minimum spanning trees"

Scopo del progetto è l'implementazione dell'algoritmo di Prim per la soluzione del problema MST per grafi non-diretti e connessi con pesi non negativi.
Di seguito le indicazioni per poter utilizzare l'API risolutiva in Prolog.


Interfaccia Prolog per la manipolazione di grafi.

	new_graph(G).
		inserisce il grafo G nella base dati; se esiste già non fa nulla

	delete_graph(G).
		rimuove tutto il grafo (vertici e archi inclusi) dalla base dati

	new_vertex(G, V).
		inserisce il vertice V del grafo G nella base dati; se esiste già non fa nulla

	graph_vertices(G, Vs).
		è vero quando Vs è una lista contente tutti i vertici di G; fallisce se il grafo non esiste

	list_vertices(G).
		stampa alla console una lista dei vertici del grafo G; fallisce se il grafo non esiste

	new_arc(G, U, V) :-
		new_arc(G, U, V, 1).
	new_arc(G, U, V, Weight).
		inserisce l'arco di peso Weight tra i vertici U e V del grafo G nella base dati, sovrascrivendo altri eventuali archi già presenti tra i due vertici

	graph_arcs(G, Es).
		è vero quando Vs è una lista contente tutti gli archi di G; fallisce se il grafo non esiste

	vertex_neighbors(G, V, Ns).
		è vero quando V è un vertice di G e Ns è una lista contenente gli archi che portano ai vertici immediatamente raggiungibili da V

	adjs(G, V, Vs).
		è vero quando V è un vertice di G e Vs è una lista contenente i vertici ad esso adiacenti

	list_arcs(G).
		stampa alla console una lista degli archi del grafo G; fallisce se il grafo non esiste

	list_graph(G).
		stampa alla console una lista dei vertici e degli archi del grafo G; fallisce se il grafo non esiste

	read_graph(G, FileName).
		legge un grafo G da un file FileName e lo inserisce nella base dati; il file deve essere del tipo CSV con per ogni riga vertice1 vertice2 e peso separati da un carattere di tabulazione

	write_graph(G, FileName) :-
		write_graph(G, FileName, graph).
	write_graph(G, FileName, Type).
		è vero quando il grafo G viene scritto nel formato descritto in read_graph sul file FileName secondo il valore di Type; se Type è "graph" allora G è un grafo della base dati; se Type è "edges" allora G è una lista di archi


MST in Prolog.
	
	vertex_key(G, V, K).
		è vero quando V è un vertice di G e, durante e dopo l'esecuzione dell'algoritmo di Prim, contiene il peso minimo K di un arco che connetto V nell'albero minimo; se questo arco non esiste (ed all'inizio dell'esecuzione) allora K è inf

	vertex_previous(G, V, U).
		è vero quando V ed U sono vertici di G e, durante e dopo l'esecuzione dell'algoritmo di Prim, il vertice U è il vertice "genitore" di V nel MST

	mst_prim(G, Source).
		ha successo con un effetto collaterale; dopo la sua prova, la base dati ha al suo interno i predicati vertex-key e vertex-previous per ogni vertice del grafo G, calcolati tramite l'algoritmo di Prim partendo dal vertice Source

	mst_get(G, Source, PreorderTree).
		è vero quando PreorderTree è una lista di archi del MST ordinata secondo un attraversamento preorder dello stesso, fatta rispetto al peso dell'arco; archi con pari peso vengono ordinati lessicograficamente rispetto al nome del vertice target


MinHeap in Prolog.

	new_heap(H).
		inserisce un nuovo heap H nella base dati; se esiste già non fa nulla

	delete_heap(H).
		rimuove tutto lo heap (incluse tutte le "entries") dalla base dati

	heap_has_size(H, S).
		è vero quando lo heap H esiste e la dimensione corrente è S

	heap_empty(H).
		è vero quando lo heap H esiste ed è vuoto

	heap_not_empty(H).
		è vero quando lo heap H esiste e non è vuoto

	heap_head(H, K, V).
		è vero quando l'elemento dello heap H con chiave minima K è V

	heap_insert(H, K, V).
		è vero quando l'elemento V è inserito correttamente nello heap H con chiave K

	heap_extract(H, K, V).
		è vero quando la coppia K, V con K minima, è rimossa dallo heap H

	modify_key(H, NewKey, OldKey, V).
		è vero quando la chiave OldKey associata al valore V è sostituita da NewKey

	list_heap(H).
		stampa alla console lo stato interno dello heap H; fallisce se lo heap H non esiste


