# Programming languages

Implementation of Prim's algorithm using Prolog and Lisp programming languages. Aim of the implementation is to solve the MST problem for undirected and connected graphs with non-negative weights.